import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jikoni/commons/dropdown_list.dart';
import 'package:jikoni/commons/personalised_bottom_bar_navigation_item.dart';
import 'package:jikoni/commons/search_by_ingredient.dart';
import 'package:jikoni/commons/search_field.dart';
import 'package:jikoni/create_recipe.dart';
import 'package:jikoni/model/ingredient.dart';
import 'package:jikoni/recipe_presentation.dart';
import 'package:jikoni/model/recipe.dart';
import 'package:jikoni/services/recipe_service.dart';

/// Recipe list
/// We have many card. Each card represent a recipe
/// Each card has an image and the label of the recipe
class RecipeList extends StatefulWidget{

  List<Recipe> listRecipe = List.empty(growable: true);
  
  RecipeList(){
    //init();
  }

  void init(){
    listRecipe.add(
        Recipe(
          image: "https://liliebakery.fr/wp-content/uploads/2021/04/Tarte-aux-fraises-cyril-lignac-Lilie-Bakery-500x500.jpg",
          label: "Tarte à la fraise",
          author: "Author 1",
          description: "Faire la pâte sablée en mélangeant les ingrédients. L'étaler dans un moule. Piquer avec une fourchette. La faire cuire pendant",
          ingredients: List.filled(2, Ingredient("Farine", 2, "g"))
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://img.cuisineaz.com/610x610/2014/03/18/i93668-tarte-a-l-orange-sucre.jpg",
            label: "Tarte à l'orange",
            author: "Author 1",
            description: "Etaler ou dérouler la pâte sablée ou brisée et la disposer au fond du moule. Couper les oranges sauf une en très fines rondelles. Préchauffer le four à 170 °C...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://assets.afcdn.com/recipe/20200828/113345_w1024h768c1cx2880cy1920.jpg",
            label: "Gâteau au yaourt",
            author: "Author 1",
            description: "Préchauffer le four à 180°C (thermostat 6). Mélanger tout simplement les ingrédients un à un en suivant l'ordre suivant : levure, yaourt, huile, sucre, farine, oeufs et zeste de citron. Beurrer ou huilez un moule à manqué à l'aide d'une feuille...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://www.atelierdeschefs.com/media/recette-e22147-fondant-au-chocolat-coeur-de-caramel-au-beurre-demi-sel-coulis-de-framboises.jpg",
            label: "Recette de Fondant au chocolat",
            author: "Author 2",
            description: "Placer le sucre dans une poêle ou une casserole large. Faire chauffer à feu vif et ne commencer à mélanger que lorsqu'on obtient un début de couleur caramel. Remuer alors vivement avec une spatule jusqu'à ce que le caramel soit bien homogène...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://cuisinedumboa.com/wp-content/uploads/2019/03/FB_IMG_15307324283946846-1.jpg",
            label: "ERU",
            author: "Author 2",
            description: "Lavez et découpez tous vos légumes (Eru). Après avoir découpé et lavé la peau de bœuf, faites la bouillir pendant 30 minutes ensuite ajouter la viande de bœuf préalablement nettoyée et découpée. N’oubliez pas d’y ajouter du sel...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://reglo.org/admin/posts_images/5901_6_2017-03-14consommons-local-tous-dans-la-sauce-jaune.jpg",
            label: "Taro avec sa sauce jaune",
            author: "Author 2",
            description: "Fais cuire le taro en robe des champs dans une casserole pendant environ 2h. Épluche les taros et pile-les pendant qu'ils sont encore chauds. Assure-toi qu'il n'y a plus de grumeaux en les écrasant, la pâte doit être lisse. Ajoute un peu d’eau à...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "http://cdn.shopify.com/s/files/1/0046/8687/2649/articles/20180428-224841_grande_da48bffe-31ad-4658-b9af-383f745ea8b3.jpg?v=1550656688",
            label: "Okok sucré",
            author: "Author 1",
            description: "Préparez vos feuilles d’okok et réservez. Versez le suc de noix de palme, l’eau et le sel. Laissez cuire pendant 5 à 10 minutes. Ajoutez la pâte d’arachide...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://s3-eu-west-1.amazonaws.com/images-ca-1-0-1-eu/recipe_photos/original/85533/_ndolee.png",
            label: "Ndolè",
            author: "Author 3",
            description: "Mettre l’eau salée à bouillir, découper et laver les feuilles de ndole. Les mettre dans de l’eau bouillante et les cuire rapidement à découvert pendant 45mn environ avec un morceau de sel gemme.",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://img.cuisineaz.com/660x660/2015/07/31/i95030-poulet-basquaise.jpg",
            label: "Poulet basquaise",
            author: "Author 3",
            description: "Épluchez les oignons et les gousses d'ail. Lavez soigneusement les cuisses de poulet ainsi que les poivrons et les tomates. Dans une marmite, rissolez les morceaux de...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://www.recetteaz.net/wp-content/uploads/2020/01/Recette-Poisson-brais%C3%A9-1024x768.jpg",
            label: "Poisson braisé",
            author: "Author 3",
            description: "Nettoyer et laver minutieusement les poissons. Hacher un oignon, le mélanger avec une gousse d’ail écrasée et le gingembre. Incorporer le piment, un cube d’assaisonnement et mélanger avec 3 c.à.s d’huile. Enduire ce mélange à l’intérieur...",
            ingredients: List.empty(growable: true)
        )
    );
    listRecipe.add(
        Recipe(
            image: "https://fac.img.pmdstatic.net/fit/https.3A.2F.2Fi.2Epmdstatic.2Enet.2Ffac.2F2022.2F01.2F27.2Fc61aaf77-2e2a-4970-80db-0669a2429a6b.2Ejpeg/750x562/quality/80/crop-from/center/cr/wqkgSVNUT0NLL0dFVFRZIElNQUdFUyAvIEZlbW1lIEFjdHVlbGxl/couscous-algerien.jpeg",
            label: "Couscous algérien",
            author: "Author 3",
            description: "Faites revenir les morceaux d’agneau avec un filet d’huile d’olive dans la marmite du couscoussier. Préchauffez le four à 180°C. Ajoutez les oignons mixés et laissez dorer pendant 10 minutes en retournant les morceaux de viande d'agneau...",
            ingredients: List.empty(growable: true)
        )
    );
  }

  @override
  State<RecipeList> createState() => _RecipeListState();
}

class _RecipeListState extends State<RecipeList> {
  // use widget. to get data from the parent

  List<String> filterByIngredients = List.empty(growable: true);
  List<String> ingredientsList = List.empty(growable: true);

  var _limit = 20;   // limit of recipes per page
  var _currentPage = 0;  // The current page number
  var _numberOfPage = 1; // The remaining number of pages to display

  var _showSearchLabelField = false;
  var _showSearchIngredientField = false;

  String filterByField = "";

  void getRecipe() async{
    var response = await RecipeService().recipeList(0, 20);
    if(response.statusCode == 200){
      json.decode(response.body)['content'].forEach((recipeDto) {
        var recipe = Recipe.deserialize(recipeDto);
        widget.listRecipe.add(recipe);
      });
    }
  }

  @override
  initState() {
    RecipeService().recipeList(0, 20).then((response) => {
      if(response.statusCode == 200){
        setState(() {
          json.decode(response.body)['content'].forEach((recipeDto) {
            var recipe = Recipe.deserialize(recipeDto);
            widget.listRecipe.add(recipe);
          });
        })
      }
    });
  }

  void filter(String value){
    filterByField = value;
    switch(filterByField){
      case "all":
        setState(() {
          _showSearchLabelField = false;
          _showSearchIngredientField = false;
        });
        break;
      case "label":
        setState(() {
          _showSearchIngredientField = false;
          _showSearchLabelField = true;
        });
        break;
      case "ingredients":
        setState(() {
          _showSearchLabelField = false;
          _showSearchIngredientField = true;
        });
        break;
    }
  }

  void searchByLabel(String label){
    if(_showSearchLabelField){
      // TODO We call the api url to get the result which correspond to label
      // TODO use setState to repaint the widget
    }
  }

  void searchByIngredients(String ingredient){
    if(_showSearchIngredientField){
      // TODO We call the api url to get the result which correspond to ingredient
      // TODO We verify if the ingredient exist if it is ok
      // TODO use setState to repaint the widget

    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            //leading: const BackButtonIcon(),
            //backgroundColor: const Color(0xFFF6B234),
            title: const Text("Liste de recettes"),
          ),
          bottomNavigationBar: PersonalisedBottomBarNavigationBar(),
          body: CustomScrollView(
            slivers: [
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    DropdownList("Trier par", const {"Tous": "all", "Nom": "label", "Ingrédients": "ingredients"},filter,),
                    if (_showSearchLabelField == true) SearchField("Saisissez le titre de la recette", searchByLabel),
                    if (_showSearchIngredientField == true) SearchField("Saisissez un ingrédient de la recette", searchByIngredients),
                    if (_showSearchIngredientField == true) SearchByIngredients(ingredientsList),
                    ...widget.listRecipe.map((recipe) => RecipePresentation(recipe),),
                    if(_currentPage <_numberOfPage) Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(bottom: 15),
                      child: OutlinedButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Icon(
                              Icons.search,
                              color: Color.fromARGB(180, 0, 0, 0),
                            ),
                            Text("Plus de résultats",style: TextStyle(
                                color: Color.fromARGB(180, 0, 0, 0),
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          // TODO load more recipes
                        },
                      ),
                    )
                  ]
                ),
              ),
            ],
          ),
          floatingActionButton: Builder(
            builder: (context) => FloatingActionButton.extended(
              label: const Text(""),
              icon: const Icon(Icons.add),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipe()));
              },
            ),
          ),
        ),
      ),
    );
  }
}