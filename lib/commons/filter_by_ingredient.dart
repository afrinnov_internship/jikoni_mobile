import 'package:flutter/material.dart';
import 'package:jikoni/commons/search_by_ingredient.dart';
import 'package:jikoni/commons/search_field.dart';


/// This widget has the textFormField and the chips. Each chip represent an ingredient
class FilterByIngredientsWidget extends StatefulWidget{

  Function searchByIngredients;
  List<String> ingredientsList;

  FilterByIngredientsWidget(this.ingredientsList, this.searchByIngredients, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StateFilterByIngredients();

}

class _StateFilterByIngredients extends State<FilterByIngredientsWidget>{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SearchField("Saisissez un ingrédient de la recette", widget.searchByIngredients),
        SearchByIngredients(widget.ingredientsList),
      ],
    );
  }


}