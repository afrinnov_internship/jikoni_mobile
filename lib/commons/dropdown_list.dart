import 'package:flutter/material.dart';


class DropdownList extends StatefulWidget{

  String label; // The text which will be display before the dropdownList
  Map<String, String> dropdownListOptions;
  Function selectHandler;

  DropdownList(this.label, this.dropdownListOptions, this.selectHandler, {Key? key}) : super(key: key);

  @override
  State<DropdownList> createState() => _DropdownListState() ;

}

class _DropdownListState extends State<DropdownList>{

  var _dropdownValue = "all";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      padding: const EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 15),
            child: Text(widget.label),
          ),
          DropdownButton<String>(
            value: _dropdownValue,
            items: [
              ...widget.dropdownListOptions.keys.map<DropdownMenuItem<String>>((String key) {
                return DropdownMenuItem(value: widget.dropdownListOptions[key], child: Text(key),);
              })
            ],
            onChanged: (String? value) {
              setState((){
                _dropdownValue = value!;
              });
              widget.selectHandler(value);
            },
          ),
        ],
      ),
    );
  }

}