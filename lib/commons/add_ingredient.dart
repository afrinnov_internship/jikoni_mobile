import 'package:flutter/material.dart';

class AddIngredient extends StatefulWidget{

  Function submitHandler;

  AddIngredient(this.submitHandler, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StateAddIngredient();

}

class _StateAddIngredient extends State<AddIngredient>{

  final List<String> _units = ["mg", "g", "kg", "ml", "L", "unite"];
  String _currentUnit = "g";

  final _ingredientFormKey = GlobalKey<FormState>();

  TextEditingController _labelFieldController = TextEditingController();
  TextEditingController _quantityFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _ingredientFormKey,
      child: Column(
        children: [
          Wrap(
            alignment: WrapAlignment.center,
            children: [
              Container(
                width: double.infinity,
                margin: const EdgeInsets.fromLTRB(35, 5, 35, 5),
                child: TextFormField(
                  controller: _labelFieldController,
                  validator: (value) {
                    if (value == null || value.isEmpty){
                      return "Veuillez entrer le nom de l'ingrédient";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Nom Ingrédient",
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * .5,
                margin: const EdgeInsets.fromLTRB(0, 5, 5, 5),
                child: TextFormField(
                  controller: _quantityFieldController,
                  validator: (value) {
                    if (value == null || value.isEmpty){
                      return "Veuillez entrer la quantité";
                    }
                    if(num.tryParse(value) == null) return "veuillez entrer un nombre";
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Quantité",
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * .28,
                margin: const EdgeInsets.only(left: 10),
                child: Column(
                  children: [
                    DropdownButtonFormField(
                      validator: (String? value) {
                        if (value == null || value.isEmpty){
                          return "Veuillez sélectionnez une unité";
                        }
                      },
                      decoration: const InputDecoration(
                        labelText: "Unité",
                      ),
                      items: [
                        ..._units.map<DropdownMenuItem<String>>((String unit) {
                          return DropdownMenuItem(value: unit, child: Text(unit),);
                        })
                      ],
                      onChanged: (String? value) {
                        setState(() {
                          _currentUnit = value!;
                        });
                      }
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                child: ElevatedButton(
                    onPressed: () async {
                      if(_ingredientFormKey.currentState!.validate()){
                        widget.submitHandler(
                            _labelFieldController.value.text,
                            num.parse(_quantityFieldController.value.text),
                            _currentUnit,
                        );
                        _labelFieldController.clear();
                        _quantityFieldController.clear();
                      }
                    },
                    child: const Text("Ajouter l'ingrédient")
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}