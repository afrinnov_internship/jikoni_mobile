import 'package:flutter/material.dart';


class PersonalisedBottomBarNavigationBar extends StatefulWidget {

  PersonalisedBottomBarNavigationBar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StatePersonalisedBottomBarNavigationBar();

}

class _StatePersonalisedBottomBarNavigationBar extends State<PersonalisedBottomBarNavigationBar>{

  int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _onItemTapped,
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: "Accueil",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.food_bank_outlined),
          label: "Recettes",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_rounded),
          label: "Profil",
        ),
      ]
    );

  }

  void _onItemTapped(int index){
    setState(){
      _selectedIndex = index;
    }
  }

}