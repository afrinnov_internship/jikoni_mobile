import 'package:flutter/material.dart';

class SearchByIngredients extends StatefulWidget{

  List<String> ingredientsList = List.empty(growable: true);

  SearchByIngredients(this.ingredientsList, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StateSearchByIngredients();

}

class _StateSearchByIngredients extends State<SearchByIngredients>{
  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        ...widget.ingredientsList.map((ingredient) => Chip(
            label: Text(ingredient),
            deleteIcon: const Icon(Icons.delete),
            onDeleted: () {
              setState(() {
                widget.ingredientsList.remove(ingredient);
              });
            },
          ),
        ),
      ],
    ) ;
  }

}