import 'package:flutter/material.dart';

class SearchField extends StatelessWidget{

  String label;
  Function submitHandler;

  final textFormController = TextEditingController();
  SearchField(this.label, this.submitHandler, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(left: 40,right: 40, bottom: 20),
      child: TextFormField(
        controller: textFormController,
        //expands: true,
        decoration: InputDecoration(
          border: const UnderlineInputBorder(),
          labelText: label,
          suffixIcon: IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              submitHandler(textFormController.text);
            },
          )

        ),
        onFieldSubmitted: (String? value) {
          submitHandler(value);
        },
      ),
    );
  }

}