import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:jikoni/api/apiUrl.dart';

class RecipeService{

  Future<http.Response> createRecipe(Map<String, dynamic> recipe){
    return http.post(
        Uri.parse(ApiUrl.createRecipe),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(recipe)
    );
  }

  Future<http.Response> recipeList(int page, int length){
    return http.get(Uri.parse("${ApiUrl.recipeList}/$page/$length"));
  }

  Future<http.Response> detailRecipe(String recipeCode){
    return http.get(Uri.parse("${ApiUrl.detailRecipe}/$recipeCode"));
  }
}