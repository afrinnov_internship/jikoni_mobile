

class ApiUrl{
  static const _host = "192.168.0.105";
  static const _port = "8081";
  static const recipeList = "http://$_host:$_port/api/v1/recipe";
  static const detailRecipe = "http://$_host:$_port/api/v1/recipe/details";
  static const createRecipe = "http://$_host:$_port/api/v1/recipe/create";
}