import 'dart:collection';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:jikoni/commons/add_ingredient.dart';
import 'package:jikoni/model/ingredient.dart';
import 'package:jikoni/model/recipe.dart';
import 'package:jikoni/services/recipe_service.dart';


class CreateRecipe extends StatefulWidget {

  CreateRecipe({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StateCreateRecipe();

}

class _StateCreateRecipe extends State<CreateRecipe>{

  // the key is the name of the ingredient and the value is ingredient's quantity
  List<Ingredient> _ingredientList = List.empty(growable: true);
  TextEditingController _descriptionCtrl = TextEditingController();
  TextEditingController _labelCtrl = TextEditingController();
  PlatformFile? _file;
  bool _isFormSubmit = false;
  final _formKey = GlobalKey<FormState>();

  void addIngredient(String name, double quantity, String unit){
    setState(() {
      _ingredientList.add(Ingredient(name, quantity, unit));
    });
  }

  void deleteIngredient(Ingredient ingredient){
    setState(() {
      _ingredientList.remove(ingredient);
    });
  }

  void pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.image);
    if(result == null) return ;
    _file = result.files.first;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: const Text("Enregistrer une recette"),
        ),
        body: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  if(_ingredientList.isEmpty && _isFormSubmit) Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 25, bottom: 15),
                    child: const Text("Veuillez ajouter des ingrédients", style: TextStyle(color: Colors.redAccent, fontSize: 16),),
                  ),
                  if(_ingredientList.isNotEmpty) DataTable(
                    columnSpacing: 28,
                    columns: const [
                      DataColumn(label: Text("Ingrédient")),
                      DataColumn(label: Text("Quantité")),
                      DataColumn(label: Text("Unité")),
                      DataColumn(label: Text("Supprimer"))
                    ],
                    rows: [
                      ..._ingredientList.map((ingredient) => DataRow(
                        cells: [
                          DataCell(
                              ConstrainedBox(
                                constraints: const BoxConstraints(maxWidth: 85),
                                child: Text(ingredient.label,overflow: TextOverflow.ellipsis,),

                              )
                          ),
                          DataCell(
                            Text(ingredient.quantity.toString()),
                          ),
                          DataCell(
                              Text(ingredient.unit)
                          ),
                          DataCell(
                              ConstrainedBox(
                                  constraints: const BoxConstraints(maxWidth: 50),
                                  child: IconButton(
                                    icon: const Icon(Icons.delete),
                                    onPressed: () {
                                      deleteIngredient(ingredient);
                                    },
                                  )
                              )
                          ),
                        ],
                      ),
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 5, bottom: 15),
                    child: AddIngredient(addIngredient),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: double.infinity,
                          //margin: const EdgeInsets.only(top: 20),
                          //alignment: Alignment.center,
                          margin: const EdgeInsets.fromLTRB(30, 0, 30, 25),
                          child: TextFormField(
                            controller: _labelCtrl,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: "Entrer le nom de la recette",
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty){
                                return "Veuillez entrer la nom de votre recette";
                              }
                              return null;
                            },
                          ),
                        ),
                        ElevatedButton(
                            onPressed: () async {
                              pickFile();
                            },
                            child: const Text("Choisissez l'image de la recette")
                        ),
                        if(_file != null) Text("${_file?.name}"),
                        if(_file == null && _isFormSubmit) const Text("Veuillez sélectionner une image", style: TextStyle(color: Colors.redAccent, fontSize: 16),),
                        Container(
                          margin: const EdgeInsets.only(left: 10,top: 15,right: 10),
                          child: TextFormField(
                            controller: _descriptionCtrl,
                            minLines: 4,
                            maxLines: 60,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Décrivez les étapes ici",
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty){
                                return "Veuillez entrer la description de votre recette";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: ElevatedButton(
                              onPressed: () async {
                                setState(() {
                                  _isFormSubmit = true;
                                });
                                if(_formKey.currentState!.validate() && _ingredientList.isNotEmpty && _file!.size != 0){
                                  var recipe = Recipe(
                                      image: _file!.name,
                                      label: _labelCtrl.value.text,
                                      description: _descriptionCtrl.value.text,
                                      ingredients: _ingredientList
                                  );
                                  var response = await RecipeService().createRecipe(recipe.serialize());
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: response.statusCode == 200 ? const Text("Recette ajouté") : const Text("Une erreur s'est produite"),
                                      duration: const Duration(milliseconds: 2000),
                                      width: double.infinity,
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 10,
                                      ),
                                      behavior: SnackBarBehavior.floating,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                    )
                                  );
                                }
                              },
                              child: const Text("Enregistrer la recette")
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}