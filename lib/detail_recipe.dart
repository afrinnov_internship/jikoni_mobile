import 'package:flutter/material.dart';

import 'model/recipe.dart';

/// This class represent the recipe's details
class DetailRecipe extends StatelessWidget{

  Recipe recipe;

  DetailRecipe(this.recipe, {Key? key}) : super(key: key);

  getRecipeComment(){
    // We get the comment of a particular recipe
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(recipe.label),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          // we display this if the user that consult the recipe's author
          actions: [
            IconButton(
              icon: const Icon(Icons.edit_rounded),
              onPressed: () {
                // TODO we go to the modification page
              },
            ),
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () {
                // TODO we call the endpoint to delete this recipe
              },
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
            label: const Text(""),
            icon: const Icon(Icons.add),
            onPressed: () {
              // TODO add a new comment
            },
        ),
        body: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 15, bottom: 10),
                    child: Column(
                     children: [
                       Text(recipe.label, style: const TextStyle(fontSize: 18),),
                       Text("Auteur: ${recipe.author}", style: const TextStyle(fontSize: 15),),
                     ],
                    )
                  ),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(8),
                    height: MediaQuery.of(context).size.width * .75,
                    child: Image.network(
                      recipe.image,
                      semanticLabel: "Image de la recette",
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 20, bottom: 5),
                    alignment: Alignment.center,
                    child: const Text("Ingrédients", style: TextStyle(fontSize: 20),),
                  ),
                  const Divider(
                    height: 5,
                    thickness: 2,
                    color: Colors.black54,
                  ),
                  ...recipe.ingredients.map((ingredient) =>
                      Container(
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: Text(
                          "${ingredient.quantity.toString()} ${ingredient.unit}  ${ingredient.label}",
                          style: const TextStyle(
                            fontSize: 17,
                          ),
                        ),
                      ),
                  ),
                  // Recipe description
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 20, bottom: 5),
                    alignment: Alignment.center,
                    child: const Text("Description", style: TextStyle(fontSize: 20),),
                  ),
                  const Divider(
                    height: 5,
                    thickness: 2,
                    color: Colors.black54,
                  ),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Text(recipe.description),
                  ),
                  // Comment description
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 20, bottom: 5),
                    alignment: Alignment.center,
                    child: const Text("Commentaires", style: TextStyle(fontSize: 20),),
                  ),
                  const Divider(
                    height: 5,
                    thickness: 2,
                    color: Colors.black54,
                  ),
                ],
              ),
            ),
          ]
        ),
      ),
    );
  }

}