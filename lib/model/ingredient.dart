
import 'dart:collection';

class Ingredient{

  String label;
  double quantity;
  String unit;
  Ingredient(this.label, this.quantity, this.unit);

  Map<String, dynamic> serialize(){
    return {
      "ingredientName": label,
      "ingredientQuantity": quantity,
      "ingredientUnit": unit
    };
  }

  static Ingredient deserialize(Map<String, dynamic> json){
    return Ingredient(
        json['ingredientName'],
        json['ingredientQuantity'],
        json['ingredientUnite'],
    );
  }
}