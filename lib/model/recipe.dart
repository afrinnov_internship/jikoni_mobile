import 'dart:convert';

import 'package:jikoni/model/ingredient.dart';

class Recipe{
  String image;
  String label;
  List<Ingredient> ingredients;
  String description;
  String? author;
  String? code;

  Recipe({required this.image, required this.label, this.author, required this.description, required this.ingredients, this.code});

  Map<String, Object> serialize(){
    return {
      "recipeImageUrl": image,
      "recipeLabel": label,
      "recipeDescription": description,
      "recipeIngredients": ingredients.map((ingredient) => ingredient.serialize()).toList()
    };
  }

  static Recipe deserialize(Map<String, dynamic> json){
    List<Ingredient> ingredients = List.empty(growable: true);
    json['recipeIngredients'].forEach(
        (ingredient) {
          ingredients.add(Ingredient.deserialize(ingredient));
        }
    );
    return Recipe(
        image: json['recipeImageUrl'],
        label: json['recipeLabel'],
        author: json['recipeAuthor'],
        description: json['recipeDescription'],
        ingredients: ingredients
    );
  }

}