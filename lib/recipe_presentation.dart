import 'package:flutter/material.dart';
import 'package:jikoni/detail_recipe.dart';
import 'package:jikoni/model/recipe.dart';

/// This class represent a row of presentation of a recipe in the recipe list
class RecipePresentation extends StatelessWidget{

  Recipe recipe;

  RecipePresentation(this.recipe, {Key? key}) : super(key: key);

  getRecipeDetail(BuildContext context){
    // TODO navigate to detail recipe page and transfer recipe to the new page
    Navigator.push(
        context,
        MaterialPageRoute(builder: (builder) => DetailRecipe(recipe))
    );
  }

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: () {
        getRecipeDetail(context);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 8),
        padding: const EdgeInsets.all(6),
        child: Row(
          children: [
            Container(
              width: 50,
              height: 40,
              margin: const EdgeInsets.only(right: 5),
              child: Image.network(
                recipe.image,
                semanticLabel: "Image de la recette",
                width: 50,
                height: 40,
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(recipe.label, semanticsLabel: "Titre de la recette",),
                  Text("De ${recipe.author}", semanticsLabel: "Auteur",),
                ],
              ),
            ),
          ],
        ),
      )

    );
  }



}